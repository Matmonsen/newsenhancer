# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-19 15:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articleScraper', '0011_auto_20180219_1555'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articleurltemplate',
            name='id_separator',
            field=models.CharField(blank=True, default='-', max_length=1),
        ),
    ]
