# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-19 15:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0001_initial'),
        ('articleScraper', '0010_auto_20180210_1422'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleUrlTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('id_position', models.IntegerField()),
                ('id_separator', models.CharField(max_length=1)),
                ('id_length', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('news_site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='scraper.NewsSite')),
            ],
        ),
        migrations.AlterField(
            model_name='revision',
            name='file',
            field=models.FilePathField(blank=True, path='/home/sindre/Documents/NewsEnhancer/sites'),
        ),
    ]
