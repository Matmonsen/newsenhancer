# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-03 13:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articleScraper', '0007_auto_20180106_1706'),
    ]

    operations = [
        migrations.AddField(
            model_name='articletemplate',
            name='selector',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
