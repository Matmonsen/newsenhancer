# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-28 22:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('headlineScraper', '0001_initial'),
        ('scraper', '0001_initial'),
        ('submission', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='headline',
            name='news_site',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='scraper.NewsSite'),
        ),
        migrations.AddField(
            model_name='headline',
            name='summary',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='summary', to='submission.HeadlineSummary'),
        ),
    ]
