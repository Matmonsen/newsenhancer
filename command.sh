drop table django_migrations;
drop table articleScraper_article cascade;
drop table articleScraper_article_images cascade;
drop table articleScraper_article_journalists cascade;
drop table articleScraper_articleimage cascade;
drop table headlineScraper_headline cascade;
drop table headlineScraper_headlinetemplate cascade;
drop table articleScraper_articletemplate cascade;
drop table articleScraper_journalist cascade;
drop table scraper_newssite cascade;
drop table headlineScraper_rank cascade;
drop table django_content_type cascade;
drop table auth_permission cascade;
drop table auth_group cascade;
drop table auth_group_permissions cascade;
drop table auth_user cascade;
drop table auth_user_groups cascade;
drop table auth_user_user_permissions cascade;
drop table django_admin_log cascade;
drop table django_session cascade;
drop table scraper_article cascade;
drop table scraper_headline cascade;
drop table scraper_article_images cascade;
drop table scraper_article_journalists cascade;
drop table scraper_articleimage cascade;
drop table scraper_articletemplate cascade;
drop table scraper_headlinetemplate cascade;
drop table scraper_journalist cascade;
drop table scraper_rank cascade;
drop table scraper_newssite cascade;

# HeadlineTemplate BT
"""
    headline = '.df-article'
    url = 'a.url'
    sub_title = 'p'
    title = 'a.url span'
    video = ''
    subscription ='df-skin-paywall-closed'
    exclude = [.]
"""

# ArticleTempalte BT
# https://www.bt.no/100Sport/fotball/Pogba-av-med-skade-da-Manchester-United-vant-i-Champions-League-comebacket-241931b.html
"""
    journalist = '.author'
    title = 'h1.article-title'
    sub_title = 'p.article-description'
    content = '.article-body'
    published = '.date-publisert'
    updated = '.date-oppdatert'
    image_attribute = 'src'
    image_text = 'div.caption'
    image_photographer = 'div.photographer'
    image_element = '.image-wrapper'
    photographer_delimeter=''
    photograp_ignore_text='{}'
    ignore_content_tag = 'aside'
    subscription = '.article-type-standard.salesposter'
    date_format = '%d. %b. %Y %H:%M'
"""

# ArticleTemplate Aftenposten
# https://www.aftenposten.no/norge/politikk/i/kk7lQ/Strid-i-Arbeiderpartiet-for-valget-om-hvordan-Oslo-makten-skulle-brukes
"""
    journalist = 'div.author-name'
    title = 'h1.article-title'
    sub_title = '.article-description'
    content = '.article-content'
    published = 'time.date.published'
    updated = 'time.date.modified'
    image_attribute = 'src'
    image_text = 'div.caption'
    image_photographer = 'div.photographer'
    image_element = '.image-wrapper'
    photographer_delimeter=''
    photograp_ignore_text='{}'
    ignore_content_tag = 'iframe'
    date_format = '%d.%b.%Y %H:%M'
    subscription = 'div.widget-fasten-salesposters'
"""

# ArticleTemplate DB
# https://www.dagbladet.no/nyheter/mor-far-og-sonn-11-dode-etter-fall-i-vulkankrater/68689062
"""
    journalist = 'span.name'
    title = 'h1.headline'
    sub_title = '.standfirst'
    content = '.lab-bodytext-content'
    published = 'time.published'
    updated = 'time.modified'
    image_attribute = 'src'
    image_text = 'figcaption.caption'
    image_photographer = ''
    image_element = 'figure'
    photographer_delimeter="Foto:"
    photograp_ignore_text='{"Vis mer"}'
    ignore_content_tag = ''
    subscription = 'div.paywall
    date_format = '%d. %B %Y kl. %H.%M'
"""

# ArticleTemplate VG not done
# http://www.vg.no/nyheter/innenriks/kreft/marthe-sundby-42-er-doed/a/24139669/
"""
    journalist = 'li.byline-author > a'
    title = 'h1.main-title'
    sub_title = 'div#preamble'
    content = '.article-body-text'
    published = '.published'
    updated = '.updated'
    image_attribute = 'src'
    image_text = 'figcaption > p'
    image_photographer = 'figcaption'
    image_element = '.dp-picture'
    photographer_delimeter=''
    photograp_ignore_text='{}'
    ignore_content_tag = 'aside'
    subscription = 'div#paywall'
    date_format = '%d.%m.%Y %H:%M'
"""
