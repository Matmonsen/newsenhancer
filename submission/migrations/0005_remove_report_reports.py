# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-03-05 15:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('submission', '0004_userreport_explanation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='report',
            name='reports',
        ),
    ]
